# Overview
PyIslamia-examples is a collection of python scripts that use [PyIslamia module](https://bitbucket.org/tatataufik/pyislamia.git). 

# Dependancy Installation
Please refer to  [PyIslamia module](https://bitbucket.org/tatataufik/pyislamia.git) for installation of dependancies.

# Usage
1. Clone the repository ``` git clone https://bitbucket.org/tatataufik/pyislamia-examples.git ```
2. Go to the folder ``` cd pyislamia-examples ```

## Date and Time Operations
```
$ python date_time_operation.py

Datetime (Now)					: 2014-03-14 15:54:33 
in Julian Day					: 2456731.162882
Datetime						: 2014-03-05 00:00:00 
in Julian Day					: 2456721.500000
Datetime with Asia/Jakarta Tz	: 2014-03-05 00:00:00 WIB 
Datetime in UTC					: 2014-03-04 17:00:00 UTC
Datetime in Asia/Makassar Tz	: 2014-03-05 01:00:00 WITA

```

## Radian and Degree Operations

```
$ python rad_deg_operation.py
Degree,Minute,Second to Degree: 107°57'33" --> 107.959167°
Degree to Radian: 107.959167° --> 1.884243 radians
Radian to Degree: 3.141593 radians --> 180.000000°
Degree to Degree,Minute,Second: 107.959167° --> 107°57'33"
```

## Moon Ecliptical Position Calculation
```
$ python moon_position.py
Date and Time							: 2014-04-02 04:00:45 WIB
Ecl Moon Position from ELP 2000-82b		: [38.619512° -0.957916° 380635.571883] 
Ecl Position from ELP 2000 (Meeus)		: [38.819328° -0.956643° 380634.711410] 
Equatorial Position						: 36.749398° 13.531598°
Observer's position						: 107°37'0" -6°57'0"
Equatorial Position (correct parallax)	: 37.320439° 13.464082°
```

## Moon Rise, Set and Transit Times
```
$ python moon_rise_set_transit.py
========calculate moon rise, set and transit times===========
Date and Time		: 2014-04-02 04:02:24 WIB
Observer's position	: 107°37'0" -6°57'0"
Moon rise time		: 2014-04-02 07:51:28 WIB 
Moon set time		: 2014-04-02 20:00:59 WIB 
Moon transit time	: 2014-04-02 13:56:36 WIB
```

## Sun Rise, Set and Transit Times
```
$ python sun_rise_set_transit.py
========calculate sun rise, set and transit times===========
Date and Time		: 2014-04-02 04:03:09 WIB
Observer's position	: 107°37'0" -6°57'0"
Observer's elevation	: 768.000000 m (above sea level)
Sun rise time		: 2014-04-02 05:48:22 WIB 
Sun set time		: 2014-04-02 17:57:58 WIB 
Sun transit time	: 2014-04-02 11:53:13 WIB
```
## Qibla Hour and Direction Calculation
```
$ python qibla_calculation.py
========calculate qibla direction and qibla hour===========
Date and Time			: 2013-01-01 00:00:00 WIB
Observer's position		: 107°37'0" -6°57'0"
Qibla from True North	: 295°10'38"
Qibla hour				: 2013-01-01 08:49:21 WIB
========Checking sun horizontal coord at qibla hour==========
Sun Horizontal Coord.	: Azimuth=295°8'30", Altitude=43°4'36"
```

## Moon Phase
```
$ python moon_phase.py
Date and Time	: 2014-04-02 04:05:52 WIB
New Moon		: 2014-03-31 01:46:17 WIB
Full Moon		: 2014-04-15 14:43:59 WIB
Next New Moon	: 2014-04-29 13:15:54 WIB
```

## Hilal Calculation
```
$ python hilal.py
Date and Time			: 2014-04-02 04:05:27 WIB
Observer's position		: 107°37'0" -6°57'0"
Observer's elevation	: 10 m (above sea level)
=======================Hilal of current month=======================
New Moon				: 2014-03-31 01:46:17 WIB
Sun Set					: 2014-03-31 17:55:02 WIB
Moon Set				: 2014-03-31 18:21:17 WIB
Hilal altitude			: 5°21'58"
Elongation				: 8°1'52"
Age of Moon				: 16 hours 8 minutes 45 seconds
======================Hilal of next month===========================
New Moon				: 2014-04-29 13:15:55 WIB
Sun Set					: 2014-04-29 17:43:08 WIB
Moon Set				: 2014-04-29 17:50:07 WIB
Hilal altitude			: 0°47'12"
Elongation				: 1°37'21"
Age of Moon				: 4 hours 27 minutes 12 seconds
```

## Prayer Times Calculation
```
$ python prayer_time.py
========calculate muslim prayer times===========
Date and Time (Now)	: 2014-04-02 04:52:29 WIB
Observer's position	: 107°34'0" -6°57'0"
Observer's elevation	: 761.000000 m (above sea level)
JD at 0H   Shubuh   Syuruq    Dzuhr     Ashr   Maghib     Isya
2456749.5 04:35:17 05:48:48 11:53:42 15:10:03 17:58:37 19:04:04
```

## Hijriyah Date
```
$ python hijri_date.py
Date and Time (Now)	: 2014-04-03 09:30:17 WIB
Observer's position	: 107°34'0" -6°57'0"
Hijriyah date		: 3 Jumadil Akhir 1435
```

