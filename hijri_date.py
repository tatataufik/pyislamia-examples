from pytz import timezone
from pyislamia.tool import frac_of_day, start_of_day
from astronomia import calendar, util
from datetime import datetime
from pyislamia import hijri
from astronomia.calendar import cal_to_jde
from pyislamia.hijri import HIJRI_EPOCH, _days_of_hijri_month, _is_hijriyah_leap_year
import numpy as np

def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime(2014,4,8)
    jd =  cal_to_jde(dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second)

    '''Observer Latitude and longitude in [degree, minute, second]'''
    dms_lon = [107, 34, 0]
    dms_lat = [-6, 57, 0]

#     dms_lat = util.d_to_dms(-6.27780)
#     dms_lon = util.d_to_dms(106.9372)
    
    ''' date format for displaying yyyy-mm-dd HH:MM:SS, 
        e.g. 2014-03-12 20:05:30 WIB'''
    dateformat="%Y-%m-%d %H:%M:%S %Z"
    """today datetime"""
    print "Date and Time (Now)\t\t\t: %s" %  tz.localize(dt).strftime(dateformat)
   
    
    '''Longitude 107 37' converted to radian'''
    obs_lon = util.d_to_r(util.dms_to_d(dms_lon[0],dms_lon[1],dms_lon[2])) 
    '''Latitude -6 57' converted to radian'''
    obs_lat = util.d_to_r(util.dms_to_d(dms_lat[0],dms_lat[1],dms_lat[2])) 
   
    print u"Observer's position\t\t\t: %d\xb0%d'%d\" %d\xb0%d'%d\"" % (dms_lon[0],dms_lon[1],dms_lon[2],dms_lat[0],dms_lat[1],dms_lat[2])
   
    [d,m,y] =hijri.get_hijriyah_date(jd, obs_lon, obs_lat, tz.utcoffset(dt), False, "MABIMS")
    print "Hijriyah date (based on Hilal Calc)\t: %d %s %d" % (d, hijri.HIJRI_MONTHS[m-1], y)
    
    [d,m,y] =hijri.estimated_hijri_ymd(jd)
    print "Hijriyah date (based on Hisab Urfi)\t: %d %s %d" % (d, hijri.HIJRI_MONTHS[m-1], y)

    
    
if __name__=="__main__":
    main()