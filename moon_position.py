from datetime import datetime
from pytz import timezone
from astronomia import calendar,util, coordinates,nutation, dynamical
from pyislamia import moon, elp82b, parallax
from pyislamia.tool import frac_of_day
from astronomia import lunar
from pyislamia import constants as c, iau2000a_nutation
from pyislamia.astroOps import calc_hour_angle
from pyislamia.aberration import annual_aberration

def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime(2014,4,8,12,0,0)
    
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second) #julian day in local_time
    
    
    
    
    utc_jd = jd - frac_of_day(tz.utcoffset(dt)) #julian_day UTC
#     delta_t = dynamical.deltaT_seconds(utc_jd)
#     utc_jd += delta_t/86400.
  
    """date time format"""
    fmt = '%Y-%m-%d'
    hour_fmt ='%H:%M:%S %Z'
    full_fmt = fmt + " " + hour_fmt
    
    """today datetime"""
    print "Date and Time\t\t\t\t: %s" %  tz.localize(dt).strftime(full_fmt)
    print "UTC jd\t\t\t\t\t: %f " % utc_jd
    
#     '''Calculation of moon ecliptical position using ELP 2000-82b'''
#     ecl_pos = elp82b.elp82b_coordinates(utc_jd)
#     ecl_pos_in_deg = map(lambda x: util.r_to_d(x), ecl_pos[:2]) \
#         + [ecl_pos[2],]
#     print u"Ecl Moon Position from ELP 2000-82b\t: [%f\xb0 %f\xb0 %f] " % (ecl_pos_in_deg[0], ecl_pos_in_deg[1], ecl_pos_in_deg[2])
    
     
    '''Calculation of moon ecliptical position using ELP 2000 (Meeus)'''
    _lunar = lunar.Lunar()
    ecl_pos = _lunar.dimension3(utc_jd)
    ecl_pos_in_deg = map(lambda x: util.r_to_d(x), ecl_pos[:2]) + [ecl_pos[2],]
    print u"Ecl Position from ELP 2000 (Meeus)\t: [%f\xb0 %f\xb0 %f] "  % (ecl_pos_in_deg[0], ecl_pos_in_deg[1], ecl_pos_in_deg[2])
    
    obliquity = iau2000a_nutation.iau2000a_mean_obliquity(utc_jd) 
    [d_psi, d_eps] = iau2000a_nutation.iau2000a_nutation(utc_jd)
    
    print "obliquity\t\t\t\t: %f" % util.r_to_d(obliquity + d_eps)
    equ_pos = coordinates.ecl_to_equ(ecl_pos[0], ecl_pos[1], obliquity + d_eps)
    equ_pos_in_deg = map(lambda x: util.r_to_d(x), equ_pos[:2])
   
    '''Observer Latitude and longitude in [degree, minute, second]'''
    dms_lon =[107, 37, 0]
    dms_lat =[-6, 57, 0]
    
    print u"Observer's position\t\t\t: %d\xb0%d'%d\" %d\xb0%d'%d\"" % (dms_lon[0],dms_lon[1],dms_lon[2],dms_lat[0],dms_lat[1],dms_lat[2])
   
    '''Longitude 107 37' converted to radian'''
    obs_lon = util.d_to_r(util.dms_to_d(dms_lon[0],dms_lon[1],dms_lon[2])) 
    '''Latitude -6 57' converted to radian'''
    obs_lat = util.d_to_r(util.dms_to_d(dms_lat[0],dms_lat[1],dms_lat[2])) 
    
    ''' elevation 768 m (above sea level)'''
    height_msl = 768
    
    print u"Observer's elevation\t\t\t: %f m (above sea level)" % height_msl
   
    
    '''Aberration calculation'''
    d_equ_pos = annual_aberration(utc_jd, equ_pos)
    d_equ_pos_deg = map(lambda x: util.r_to_d(x), d_equ_pos[:2])
    print u"Equatorial Position\t\t\t: %f\xb0 %f\xb0" % (equ_pos_in_deg[0],equ_pos_in_deg[1])
    print u"Equatorial Aberration\t\t\t: %f\xb0 %f\xb0" % (d_equ_pos_deg[0],d_equ_pos_deg[1])
    equ_pos = [ equ_pos[i] + d_equ_pos[i] for i in range(0,2) ]
    equ_pos_in_deg = map(lambda x: util.r_to_d(x), equ_pos[:2])
    print u"Equatorial Position (correct aberration): %f\xb0 %f\xb0" % (equ_pos_in_deg[0],equ_pos_in_deg[1])
    
    '''hour angle'''
    hr_ang = calc_hour_angle(utc_jd, obs_lon, equ_pos[0])
    d_equ_pos = parallax.geocentric_parallax(hr_ang, equ_pos[1], ecl_pos[2]/c.AU,  obs_lat, height_msl)
    equ_pos = [ equ_pos[i] + d_equ_pos[i] for i in range(0,2) ]
    equ_pos_in_deg = map(lambda x: util.r_to_d(x), equ_pos[:2])
    print u"Equatorial Position (correct parallax)\t: %f\xb0 %f\xb0" % (equ_pos_in_deg[0],equ_pos_in_deg[1])
    
    
        
if __name__ == '__main__':
    main()
