from datetime import datetime
from pytz import timezone
from astronomia import calendar,util
from pyislamia import moon
from pyislamia.tool import frac_of_day, jd_to_datetime, start_of_day


def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime.today()
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second) #julian day in local_time
    utc_jd = jd - frac_of_day(tz.utcoffset(dt)) #julian_day UTC
  
    """date time format"""
    fmt = '%Y-%m-%d'
    hour_fmt ='%H:%M:%S %Z'
    full_fmt = fmt + " " + hour_fmt
    
    print "========calculate moon rise, set and transit times==========="
    
    """today datetime"""
    print "Date and Time\t\t: %s" %  tz.localize(dt).strftime(full_fmt)
    
    
    '''Observer Latitude and longitude in [degree, minute, second]'''
    dms_lon =[107, 37, 0]
    dms_lat =[-6, 57, 0]
    
    ''' date format for displaying yyyy-mm-dd HH:MM:SS, 
        e.g. 2014-03-12 20:05:30 WIB'''
    dateformat="%Y-%m-%d %H:%M:%S %Z"
    
    
    '''Longitude 107 37' converted to radian'''
    obs_lon = util.d_to_r(util.dms_to_d(dms_lon[0],dms_lon[1],dms_lon[2])) 
    '''Latitude -6 57' converted to radian'''
    obs_lat = util.d_to_r(util.dms_to_d(dms_lat[0],dms_lat[1],dms_lat[2])) 
    

    print u"Observer's position\t: %d\xb0%d'%d\" %d\xb0%d'%d\"" % (dms_lon[0],dms_lon[1],dms_lon[2],dms_lat[0],dms_lat[1],dms_lat[2])
   
    rise_set = moon.moonRiseSet(jd,obs_lon,obs_lat, tz.utcoffset(dt))
    
    '''julian day at 0 H of the current day'''
    jd_start_of_day = start_of_day(jd) 
    
    
    '''rise and set times in julian day'''
    rise_set = [t + jd_start_of_day  if t is not None else None for t in rise_set]
    ''' convert to date_time'''
    rise_set = [jd_to_datetime(jd) if jd is not None else None for jd in rise_set]
    print "Moon rise time\t\t: %s " % (tz.localize(rise_set[0]).strftime(full_fmt) if rise_set[0] is not None else "-")
    print "Moon set time\t\t: %s " % (tz.localize(rise_set[1]).strftime(full_fmt) if rise_set[0] is not None else "-")
    
    '''calculate transit time'''
    transit = moon.moonTransit(jd,obs_lon,obs_lat, tz.utcoffset(dt)) + jd_start_of_day
    transit_dt = jd_to_datetime(transit)
    print "Moon transit time\t: %s" % (tz.localize(transit_dt).strftime(full_fmt) if transit_dt is not None else "-")
    
    
    
    
if __name__=="__main__":
    main()
    