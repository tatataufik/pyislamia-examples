from astronomia import util,constants
from pyislamia.tool import d_to_dms

def main():
	'''degree value from 107 57' 33" '''
	deg = util.dms_to_d(107, 57, 33)
	print u"Degree,Minute,Second to Degree: %d\xb0%d'%d\" --> %f\xb0" \
		% (107,57,33, deg)
	
	'''convert deg to radian'''
	rad = util.d_to_r(deg)
	print u"Degree to Radian: %f\xb0 --> %f radians" % (deg, rad)
	
	'''convert phi radian to deg'''
	rad_to_deg = util.r_to_d(constants.pi)
	
	print u"Radian to Degree: %f radians --> %f\xb0" % (constants.pi, rad_to_deg)
	
	'''convert degree to degree minute second'''
	dms = d_to_dms(deg)
	
	print u"Degree to Degree,Minute,Second: %f\xb0 --> %d\xb0%d'%d\"" \
		% (deg,dms[0],dms[1],dms[2])
	
if __name__ == "__main__":
	main()
	